package edu.khtn.qly_sinhvien;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    static String path = "D:/DsSinhVien.txt";
    static DsSVien dssv = new DsSVien();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean isNumber(String number){
        try{
            Integer.parseInt(number);
        }catch (NumberFormatException e){
            System.out.println("Lỗi! Hãy nhập số.");
            return false;
        }return true;
    }

    public void ghiFile(View v){
        try {
            OutputStreamWriter objOut = new OutputStreamWriter
                    (new BufferedOutputStream(new FileOutputStream(path)));
            dssv = createDSSV();
            String strSV = "";
            String strDSSV = "";
            for (SinhVien sv : dssv.getDssv()) {
                strSV += sv.getTenSV()+"-";
                strSV += sv.getDiaChi()+"-";
                strSV += sv.getDiemSo();
                strDSSV += strSV+",";
                Log.d("DSSV",sv.toString());
            }objOut.write(strDSSV);
            Log.d("DSSV",strDSSV);
            objOut.close();
        } catch (FileNotFoundException e){
        } catch (IOException e){
        }
    }

    public DsSVien docFile(View v){
        try {
            InputStreamReader objIn = new InputStreamReader
                    (new BufferedInputStream(new FileInputStream(path)));
            dssv = new DsSVien();
            SinhVien sv = new SinhVien();
            for (String s : objIn.toString().split(",")) {
                sv.setTenSV(s.split("-")[1]);
                sv.setDiaChi(s.split("-")[2]);
                sv.setDiemSo(Integer.parseInt(s.split("-")[3]));
                dssv.addSV(sv);
            }return dssv;
        }catch (FileNotFoundException e){
            return null;
        }catch (IOException e){
            return null;
        }
    }

    public void showDSSV(View v){
        TextView tenSV0 = (TextView) findViewById(R.id.tenSV0);
        TextView tenSV1 = (TextView) findViewById(R.id.tenSV1);
        TextView tenSV2 = (TextView) findViewById(R.id.tenSV2);
        TextView diaChi0 = (TextView) findViewById(R.id.diaChi0);
        TextView diaChi1 = (TextView) findViewById(R.id.diaChi1);
        TextView diaChi2 = (TextView) findViewById(R.id.diaChi2);
        TextView diemSo0 = (TextView) findViewById(R.id.diemSo0);
        TextView diemSo1 = (TextView) findViewById(R.id.diemSo1);
        TextView diemSo2 = (TextView) findViewById(R.id.diemSo2);
        if(dssv.size()>0) {
            tenSV0.setText(dssv.getDssv().get(0).getTenSV().toString() + "");
            diaChi0.setText(dssv.getDssv().get(0).getDiaChi().toString() + "");
            diemSo0.setText(dssv.getDssv().get(0).getDiemSo() + "");
        }
        if(dssv.size()>1) {
            tenSV1.setText(dssv.getDssv().get(1).getTenSV().toString() + "");
            diaChi1.setText(dssv.getDssv().get(1).getDiaChi().toString() + "");
            diemSo1.setText(dssv.getDssv().get(1).getDiemSo() + "");
        }
        if(dssv.size()>2) {
            tenSV2.setText(dssv.getDssv().get(2).getTenSV().toString() + "");
            diaChi2.setText(dssv.getDssv().get(2).getDiaChi().toString() + "");
            diemSo2.setText(dssv.getDssv().get(2).getDiemSo() + "");
        }
    }

    public DsSVien createDSSV(){
        dssv = new DsSVien();
        SinhVien sv = new SinhVien("Tran Huy Tai","Ho Chi Minh",200);
        SinhVien sv1 = new SinhVien("Tran Huy Loc","Phan Thiet",180);
        SinhVien sv2 = new SinhVien("Tran Thanh Hien","Binh Thuan",180);
        dssv.addSV(sv);
        dssv.addSV(sv1);
        dssv.addSV(sv2);
        return dssv;
    }

    public void addSV(View v){
        EditText editTenSV = (EditText) findViewById(R.id.editTenSV);
        EditText editDiaChi = (EditText) findViewById(R.id.editDiaChi);
        EditText editDiemSo = (EditText) findViewById(R.id.editDiemSo);
        if(String.valueOf(editTenSV.getText()).isEmpty()==false&&
                String.valueOf(editDiaChi.getText()).isEmpty()==false&&String.valueOf(editDiemSo.getText()).isEmpty()==false){
            SinhVien sv = new SinhVien();
            sv.setTenSV(String.valueOf(editTenSV.getText()));
            sv.setDiaChi(String.valueOf(editDiaChi.getText()));
            sv.setDiemSo(Integer.parseInt(String.valueOf(editDiemSo.getText())));
            editTenSV.setText("");
            editDiaChi.setText("");
            editDiemSo.setText("");
            dssv.addSV(sv);
        }
    }

    public void deleteDSSV(View v){
        TextView tenSV0 = (TextView) findViewById(R.id.tenSV0);
        TextView tenSV1 = (TextView) findViewById(R.id.tenSV1);
        TextView tenSV2 = (TextView) findViewById(R.id.tenSV2);
        TextView diaChi0 = (TextView) findViewById(R.id.diaChi0);
        TextView diaChi1 = (TextView) findViewById(R.id.diaChi1);
        TextView diaChi2 = (TextView) findViewById(R.id.diaChi2);
        TextView diemSo0 = (TextView) findViewById(R.id.diemSo0);
        TextView diemSo1 = (TextView) findViewById(R.id.diemSo1);
        TextView diemSo2 = (TextView) findViewById(R.id.diemSo2);
        EditText editTenSV = (EditText) findViewById(R.id.editTenSV);
        EditText editDiaChi = (EditText) findViewById(R.id.editDiaChi);
        EditText editDiemSo = (EditText) findViewById(R.id.editDiemSo);
        if(dssv.size()>0) {
            tenSV0.setText("");
            diaChi0.setText("");
            diemSo0.setText("");
        }
        if(dssv.size()>1) {
            tenSV1.setText("");
            diaChi1.setText("");
            diemSo1.setText("");
        }
        if(dssv.size()>2) {
            tenSV2.setText("");
            diaChi2.setText("");
            diemSo2.setText("");
        }
        editTenSV.setText("");
        editDiaChi.setText("");
        editDiemSo.setText("");
        dssv.getDssv().clear();
    }
}
