package edu.khtn.qly_sinhvien;

/**
 * Created by Tai on 14/10/2015.
 */
public class SinhVien {
    String tenSV;
    String diaChi;
    int diemSo;

    public SinhVien(){
        super();
    }

    public SinhVien(String tenSV,String diaChi,int diemSo){
        super();
        this.tenSV = tenSV;
        this.diaChi = diaChi;
        this.diemSo = diemSo;
    }

    public String getTenSV() {
        return tenSV;
    }

    public void setTenSV(String tenSV) {
        this.tenSV = tenSV;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public int getDiemSo() {
        return diemSo;
    }

    public void setDiemSo(int diemSo) {
        this.diemSo = diemSo;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return tenSV+"   "+diaChi+"  "+diemSo;
    }
}
