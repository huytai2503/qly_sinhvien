package edu.khtn.qly_sinhvien;

import java.util.ArrayList;

/**
 * Created by Tai on 14/10/2015.
 */
public class DsSVien {
    ArrayList<SinhVien> dssv = new ArrayList<>();

    public DsSVien(){
        super();
    }

    public DsSVien(ArrayList<SinhVien> dssv){
        super();
        this.dssv = dssv;
    }

    public ArrayList<SinhVien> getDssv() {
        return dssv;
    }

    public void setDssv(ArrayList<SinhVien> dssv) {
        this.dssv = dssv;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {return "DSSV: ["+dssv+"]";}

    public void addSV(SinhVien sv){
        this.dssv.add(sv);
    }

    public  int size(){return dssv.size();}
}
